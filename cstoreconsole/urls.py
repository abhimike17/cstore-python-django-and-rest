from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings

from cstoreapp import views, apis

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', views.home, name='home'),

                  # CStore URL
                  path('cstore/sign_in/', auth_views.login,
                       {'template_name': 'cstore/sign_in.html'},
                       name='cstore-sign-in'),
                  path('cstore/sign_out/', auth_views.logout,
                       {'next_page': '/'},
                       name='cstore-sign-out'),
                  path('cstore/sign_up/', views.cstore_sign_up,
                       name='cstore-sign-up'),

                  path('cstore/account/', views.cstore_account, name='cstore-account'),
                  path('cstore/product/', views.cstore_product, name='cstore-product'),
                  path('cstore/product/add', views.cstore_add_product, name='cstore-add-product'),
                  path('cstore/product/edit/<int:product_id>/', views.cstore_edit_product, name='cstore-edit-product'),
                  path('cstore/order/', views.cstore_order, name='cstore-order'),
                  path('cstore/report/', views.cstore_report, name='cstore-report'),
                  path('cstore/reward/', views.cstore_reward, name='cstore-reward'),
                  path('cstore/', views.cstore_home, name='cstore-home'),

                  # Sign IN/ Sign Up/ Sign Out
                  path('api/social/', include('rest_framework_social_oauth2.urls')),

                  # API for customers
                  path('api/customer/cstores/', apis.customer_get_cstores),
                  path('api/customer/products/<int:cstore_id>/', apis.customer_get_products),
                  path('api/customer/order/add/', apis.customer_add_order),
                  path('api/customer/order/latest/', apis.customer_get_latest_order),
                  path('api/cstore/order/notification/<str:last_request_time>/', apis.cstore_order_notification),

                  path('api/cstore/order/notification/<slug:last_request_time>/', apis.cstore_order_notification),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
"""Developed by Abhijeeth
Copyrights @abhijeethbaregal@2018"""