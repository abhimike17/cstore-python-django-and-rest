"""Developed by Abhijeeth
Copyrights @abhijeethbaregal@2018"""

from django import forms

from django.contrib.auth.models import User

from cstoreapp.models import Cstore, Product


class UserForm(forms.ModelForm):
    email = forms.CharField(max_length=100, required=True)
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ("username", "password", "first_name", "last_name", "email")


class UserFormForEdit(forms.ModelForm):
    email = forms.CharField(max_length=100, required=True)

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email")


class CstoreForm(forms.ModelForm):
    class Meta:
        model = Cstore
        fields = ("name", "phone", "address", "logo")


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ('cstore',)
