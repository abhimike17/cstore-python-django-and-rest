# Generated by Django 2.0.6 on 2018-07-02 18:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cstoreapp', '0002_customer_driver'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500)),
                ('short_description', models.CharField(max_length=500)),
                ('image', models.ImageField(upload_to='product_images/')),
                ('price', models.IntegerField(default=0)),
                ('cstore', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cstoreapp.Cstore')),
            ],
        ),
    ]
