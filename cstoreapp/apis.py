"""Developed by Abhijeeth
Copyrights @abhijeethbaregal@2018"""

import json

from django.utils import timezone
from django.http import JsonResponse
from oauth2_provider.models import AccessToken
from cstoreapp.models import Cstore, Product, Order, OrderDetails
from django.views.decorators.csrf import csrf_exempt
from cstoreapp.serializers import CstoreSerializers, ProductSerializer, OrderSerializer
# import stripe
from cstoreconsole.settings import  STRIPE_API_KEY


# stripe.api_key = STRIPE_API_KEY
def customer_get_cstores(request):
    cstores = CstoreSerializers(
        Cstore.objects.all().order_by("-id"),
        many=True,
        context={"request": request}
    ).data

    return JsonResponse({"cstores": cstores})


def customer_get_products(request, cstore_id):
    products = ProductSerializer(
        Product.objects.filter(cstore_id=cstore_id).order_by("-id"),
        many=True,
        context={"request": request},
    ).data

    return JsonResponse({"products": products})


@csrf_exempt
def customer_add_order(request):
    """
    parameter :
    access token
    cstore_id
    address
    order_details(json format), example:
        [{"product_id":1, "quantity":2}, {"product_id": 2, "quantity": 3}]
    stripe token
    :param request:
    :return:
   {"status": "success"}
    """

    if request.method == "POST":
        # Get token
        access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
                                               expires__gt=timezone.now())
        # Get Profile
        customer = access_token.user.customer

        #STRIPE

        # stripe_token = request.POST["stripe_token"]

        # check if customer has any order that is not delivered
        if Order.objects.filter(customer=customer).exclude(status=Order.DELIVERED):
            return JsonResponse({"status": "failed", "error": "Complete your last order"})

        # Check Address
        if not request.POST["address"]:
            return JsonResponse({"status": "field", "error": "Address is required."})

        # Get Order details
        order_details = json.loads(request.POST["order_details"])

        order_total = 0
        for product in order_details:
            order_total += Product.objects.get(id=product["product_id"]).price * product["quantity"]

        if len(order_details) > 0:
            # Step1 - Create an Order

            # #create a charge
            # charge = stripe.charge.create(
            #     amount = order_total * 100,
            #     currency = "usd",
            #     source = stripe_token,
            #     description = "CSTORE ORDER"
            # )

            # if charge.status != "failed":
                order = Order.objects.create(
                    customer=customer,
                    cstore_id=request.POST["cstore_id"],
                    total=order_total,
                    status=Order.COOKING,
                    address=request.POST["address"]
                )

                # Step 2 - Create Order details
                for product in order_details:
                    OrderDetails.objects.create(
                        order=order,
                        product_id=product["product_id"],
                        quantity=product["quantity"],
                        sub_total=Product.objects.get(id=product["product_id"]).price * product["quantity"]
                    )
                return JsonResponse({"status": "success"})
            # else:
            #     return JsonResponse({"status":"failed", "error": "Failed connect to Stripe"})




def customer_get_latest_order(request):
    access_token = AccessToken.objects.get(token=request.GET.get("access_token"),
                                           expires__gt=timezone.now())

    customer = access_token.user.customer
    order = OrderSerializer(Order.objects.filter(customer=customer).last()).data

    return JsonResponse({"order": order})


def cstore_order_notification(request, last_request_time):
    notification = Order.objects.filter(cstore=request.user.cstore,
                                        created_at__gt=last_request_time).count()

    # select count(*) from Orders
    # where cstore = request.user.cstore AND created_at > last_request_time

    return JsonResponse({"notification": notification})
