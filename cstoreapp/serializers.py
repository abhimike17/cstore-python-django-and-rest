"""Developed by Abhijeeth
Copyrights @abhijeethbaregal@2018"""


from rest_framework import serializers

from cstoreapp.models import Cstore, \
    Product, \
    Customer, \
    Driver, \
    Order, \
    OrderDetails


class CstoreSerializers(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()

    def get_logo(self, cstore):
        request = self.context.get('request')
        logo_url = cstore.logo.url
        return request.build_absolute_uri(logo_url)

    class Meta:
        model = Cstore

        fields = ("id", "name", "phone", "address", "logo")


class ProductSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, product):
        request = self.context.get('request')
        image_url = product.image.url
        return request.build_absolute_uri(image_url)

    class Meta:
        model = Product
        fields = ("id", "name", "short_description", "image", "price")


# Order Serializer
class OrderCustomerSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source="user.get_full_name")

    class Meta:
        model = Customer
        fields = ("id", "name", "avatar", "phone", "address")


class OrderDriverSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source="user.get_full_name")

    class Meta:
        model = Customer
        fields = ("id", "name", "avatar", "phone", "address")


class OrderCstoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cstore
        fields = ("id", "name", "phone", "address")


class OrderProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("id", "name", "price")


class OrderDetailSerializer(serializers.ModelSerializer):
    product = OrderProductSerializer

    class Meta:
        model = OrderDetails
        fields = ("id", "product", "quantity", "sub_total")


class OrderSerializer(serializers.ModelSerializer):
    customer = OrderCustomerSerializer()
    driver = OrderDriverSerializer()
    cstore = OrderCstoreSerializer()
    order_details = OrderDetailSerializer(many=True)
    status = serializers.ReadOnlyField(source="get_status_display")

    class Meta:
        model = Order
        fields = ("id", "customer", "cstore", "order_details", "total", "status", "address")
