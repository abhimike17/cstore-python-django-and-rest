"""Developed by Abhijeeth
Copyrights @abhijeethbaregal@2018"""


from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from cstoreapp.forms import UserForm, CstoreForm, UserFormForEdit, ProductForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from cstoreapp.models import Product, Order


# Create your views here.

def home(request):
    return redirect(cstore_home)


@login_required(login_url='/cstore/sign_in')
def cstore_home(request):
    return redirect(cstore_order)


@login_required(login_url='/cstore/sign_in')
def cstore_account(request):
    user_form = UserFormForEdit(instance=request.user)
    cstore_form = CstoreForm(instance=request.user.cstore)

    if request.method == "POST":
        user_form = UserFormForEdit(request.POST, instance=request.user)
        cstore_form = CstoreForm(request.POST, request.FILES, instance=request.user.cstore)

        if user_form.is_valid() and cstore_form.is_valid():
            user_form.save()
            cstore_form.save()
    return render(request, 'cstore/account.html', {
        "user_form": user_form,
        "cstore_form": cstore_form
    })


@login_required(login_url='/cstore/sign_in')
def cstore_product(request):
    products = Product.objects.filter(cstore=request.user.cstore).order_by("-id")
    return render(request, 'cstore/product.html', {"products": products})


@login_required(login_url='/cstore/sign_in')
def cstore_order(request):
    if request.method == "POST":
        order = Order.objects.get(id=request.POST["id"], cstore=request.user.cstore)
        if order.status == Order.COOKING:
            order.status = Order.READY
            order.save()
    orders = Order.objects.filter(cstore=request.user.cstore).order_by("-id")
    return render(request, 'cstore/order.html', {"orders": orders})


@login_required(login_url='/cstore/sign_in')
def cstore_report(request):
    return render(request, 'cstore/report.html', {})


@login_required(login_url='/cstore/sign_in')
def cstore_reward(request):
    return render(request, 'cstore/reward.html', {})


def cstore_sign_up(request):
    user_form = UserForm()
    cstore_form = CstoreForm()

    if request.method == "POST":
        user_form = UserForm(request.POST)
        cstore_form = CstoreForm(request.POST, request.FILES)

        if user_form.is_valid() and cstore_form.is_valid():
            new_user = User.objects.create_user(**user_form.cleaned_data)
            new_cstore = cstore_form.save(commit=False)
            new_cstore.user = new_user
            # saving to database
            new_cstore.save()

            login(request, authenticate(
                username=user_form.cleaned_data["username"],
                password=user_form.cleaned_data["password"]
            ))
            return redirect(cstore_product)

    return render(request, 'cstore/sign_up.html', {
        "user_form": user_form,
        "cstore_form": cstore_form
    })


@login_required(login_url='/cstore/sign_in')
def cstore_add_product(request):
    form = ProductForm()

    if request.method == "POST":
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save(commit=False)
            product.cstore = request.user.cstore
            product.save()
            return redirect(cstore_product)

    return render(request, 'cstore/add_product.html', {
        "form": form
    })


@login_required(login_url='/cstore/sign_in')
def cstore_edit_product(request, product_id):
    form = ProductForm(instance=Product.objects.get(id=product_id))

    if request.method == "POST":
        form = ProductForm(request.POST, request.FILES, instance=Product.objects.get(id=product_id))
        if form.is_valid():
            form.save()
            return redirect(cstore_product)

    return render(request, 'cstore/edit_product.html', {
        "form": form
    })
