"""Developed by Abhijeeth
Copyrights @abhijeethbaregal@2018"""


from django.contrib import admin

# Register your models here.
from cstoreapp.models import Cstore, Customer, Driver, Product, Order, OrderDetails

admin.site.register(Cstore)
admin.site.register(Customer)
admin.site.register(Driver)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderDetails)